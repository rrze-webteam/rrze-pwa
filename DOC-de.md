## Progressive Web-App (PWA)
Dieses Plugin erweitert die aktuelle Website zu einer progressiven Web-App.

## Was ist Progressive Web-App?
Eine PWA ist eine Webanwendung (Web-App), die auf Standardwebtechnologien basiert und zusammen mit modernen Browsern einer nativen Desktop- oder Mobilanwendung ähnelt. Das Ziel ist eine Erfahrung, die so einheitlich und nahtlos ist, dass der Benutzer den Unterschied zwischen einer Web-App und einer nativen App nicht erkennen kann.

Web-Apps bieten Benutzererlebnisse durch progressive Verbesserung. Dies bedeutet im Wesentlichen, dass eine Web-App auf einem neuen Smartphone dieselben Funktionen ausführt wie auf einem Smartphone der älteren Generation. Sicher, einige Funktionen sind möglicherweise nicht verfügbar, aber die Web-App funktioniert weiterhin und funktioniert wie gewünscht.

## Was sind die Vorteile einer Web-App?
* Geschwindigkeit: Von der Installation einer We-App bis zur Verwendung geschieht alles sehr schnell. Da Daten zwischengespeichert werden können, ist es extrem schnell, die Web-App auch offline neu zu starten.
* Integrierte Benutzererfahrung: Web-App fühlen und verhalten sich wie native Anwendungen. Die Erfahrung fühlt sich nathlos und integriert an.
* Aktualisiert: Zum Senden von Updates ist keine Genehmigung des App Store erforderlich: Web-Apps sind immer auf dem neuesten Stand.
* Auffindbarkeit: Suchmaschinen können den Inhalt von Web-Apps leicht finden. Es bietet neue Möglichkeiten im Vergleich zu nativen Anwendungen. Bei Verwendung einer Web-App wird deren Inhalt in den Suchmaschinenergebnissen angezeigt.
* Verknüpfbarkeit: Jede Seite oder jeder Bildschirm der Web-App verfügt über einen direkten Link, der problemlos freigegeben werden kann.
* Engagement: Da die Web-App eventuell Benachrichtigungen an einen Benutzer senden kann, kann sie das Engagement erheblich steigern, indem sie den Benutzer benachrichtigt und mit der Web-App interagiert.

## Native Funktionen, die Web-Apps derzeit fehlen
Die Möglichkeiten, die Web-Apps bieten, sind außergewöhnlich. Es gibt jedoch einige Funktionen, die native Anwendungen vor Web-Apps stellen. Zum Beispiel Zugriff auf Kontakte, Kalender, Alarme und Browser-Lesezeichen. Mit einer Web-App ist es auch nicht möglich, Telefoniefunktionen wie das Abfangen von SMS oder Anrufen, das Senden von SMS / MMS, das Abrufen der Telefonnummer des Benutzers, das Lesen von Voicemail oder das Tätigen von Telefonanrufen zu verwenden. Wenn diese Funktionen wirklich benötigt werden, sollte das Erstellen einer nativen App in Betracht gezogen werden.

## Browser-Kompatibilität
Die Browser, die derseit Web-Apps unterstützen, sind:
* Chrome Version 81 und höher (Android und Desktop)
* Firefox Version 68 und höher (Android)
* Edge Version 81 und höher (Desktop)

## Einstellungen
Sobald das Plugin aktiviert ist, muss es im Menü Einstellungen / PWA konfiguriert werden.

Die Einstellungen generiert die erforderliche Manifest-Datei (/manifest.json) und den Service-Worker (Javascript), damit der Browser die Web-App auf dem Startbildschirm installieren kann. Eine nachfolgende Änderung der Einstellungen wird nicht sofort im Browser angezeigt. In vielen Fällen muss zusätzlich zur Deinstallation der Web-App die Registrierung des Service-Workers aufgehoben und der Browser-Cache vollständig gelöscht werden. Derzeit hat nur der Chrome Browser für Android die Funktion, die Web-App basierend auf der Manifest-Datei (/manifest.json) zu aktualisieren.

Hinweis: Solange das Plugin nicht richtig konfiguriert ist, ist die PWA-Funktion für die Website nicht verfügbar.

## Wie kann man die Registrierung des Service-Workers in Chrome für Android aufheben?
In den Browsereinstellungen sollte Folgendes ausgeführt werden:
* Auf das Browser-Menü klicken (3 Punkte in der oberen rechten Ecke des Chrome-Browsers)
* Im Menü auf Einstellungen und anschließend auf Datenschutz klicken
* Auf der Datenschutzseite am unteren Rand auf Browserdaten löschen klicken
* Auf der Registerkarte Grundlegend alle Optionen auswählen und anschließend auf Daten löschen klicken
* Wenn der Löschvorgang abgeschlossen ist, auf der Registerkarte Erweitert alle Optionen auswählen und auf Daten löschen klicken
