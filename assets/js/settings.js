jQuery(document).ready(function($) {
    $('.rrze-pwa-colorpicker').wpColorPicker();
    $('.rrze-pwa-icon-upload').click(function(e) {
        e.preventDefault();
        var rrze_pwa_media_uploader = wp.media({
                title: 'Application Icon',
                button: {
                    text: 'Select Icon'
                },
                multiple: false
            })
            .on('select', function() {
                var attachment = rrze_pwa_media_uploader.state().get('selection').first().toJSON();
                $('.rrze-pwa-icon').val(attachment.url);
            })
            .open();
    });
    $('.rrze-pwa-splash-icon-upload').click(function(e) {
        e.preventDefault();
        var rrze_pwa_media_uploader = wp.media({
                title: RRZE_PWA.mediaTitle,
                button: {
                    text: RRZE_PWA.mediaBtnText
                },
                multiple: false
            })
            .on('select', function() {
                var attachment = rrze_pwa_media_uploader.state().get('selection').first().toJSON();
                $('.rrze-pwa-splash-icon').val(attachment.url);
            })
            .open();
    });
});