if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
        navigator.serviceWorker.register(RRZE_PWA.url)
            .then(function(registration) {
                console.log('Service Worker ready.');
                registration.update();
            })
            .catch(function(error) {
                console.log('Registration failed with ' + error);
            });
    });
}