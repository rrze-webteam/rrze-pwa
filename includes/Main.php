<?php

namespace RRZE\PWA;

defined('ABSPATH') || exit;

use RRZE\PWA\Options;
use RRZE\PWA\Settings;
use RRZE\PWA\Manifest;
use RRZE\PWA\ServiceWorker;

/**
 * [Main description]
 */
class Main
{
    /**
     * [protected description]
     * @var object
     */
    protected $options;

    /**
     * [protected description]
     * @var object
     */
    protected $settings;

    /**
     * [protected description]
     * @var object
     */
    protected $manifest;

    /**
     * [protected description]
     * @var object
     */
    protected $serviceWorker;

    /**
     * [__construct description]
     */
    public function __construct()
    {
        $this->options = Options::getOptions();

        $this->settings = new Settings();
    }

    /**
     * [onLoaded description]
     * @return void
     */
    public function onLoaded()
    {
        $this->settings->onLoaded();

        if (!$this->options->enabled) {
            return;
        }

        $this->manifest = new Manifest();
        $this->serviceWorker = new ServiceWorker();

        $this->manifest->onLoaded();
        $this->serviceWorker->onLoaded();

        add_action('init', [$this, 'addRewriteRules']);
        add_action('parse_request', [$this, 'buildManifestOnRequest']);

        add_action('admin_init', [$this, 'upgrade']);
    }

    /**
     * [upgrade description]
     * @return void
     */
    public function upgrade()
    {
        $currentVersion = get_option('rrze_pwa_version');

        if (version_compare($currentVersion, plugin()->getVersion(), '==')) {
            return;
        }

        if ($currentVersion === false) {

            // Generate manifest
            $this->manifest->build();

            // Generate service worker
            $this->serviceWorker->build();

            add_option('rrze_pwa_version', plugin()->getVersion());

            return;
        }
    }

    /**
     * [addRewriteRules description]
     */
    public function addRewriteRules()
    {
        $manifestFilename = $this->manifest->getFilename();
        add_rewrite_rule(
            "^/{$manifestFilename}$",
            "index.php?{$manifestFilename}=1"
        );

        $serviceWorkerFilename = $this->serviceWorker->getFilename();
        add_rewrite_rule(
            "^/{$serviceWorkerFilename}$",
            "index.php?{$serviceWorkerFilename}=1"
        );
    }

    /**
     * [buildManifestOnRequest description]
     * @param  object $query [description]
     * @return void
     */
    public function buildManifestOnRequest($query)
    {
        if (!property_exists($query, 'query_vars') || !is_array($query->query_vars)) {
            return;
        }
        $queryVarsAsString = implode(',', $query->query_vars);
        $manifestFilename = $this->manifest->getFilename();
        $serviceWorkerFilename = $this->serviceWorker->getFilename();

        if (strpos($queryVarsAsString, $manifestFilename) !== false) {
            header('Content-Type: application/json');
            echo json_encode($this->manifest->template());
            exit();
        }

        if (strpos($queryVarsAsString, $serviceWorkerFilename) !== false) {
            header('Content-Type: text/javascript');
            echo $this->serviceWorker->template();
            exit();
        }
    }
}
