<?php

namespace RRZE\PWA;

defined('ABSPATH') || exit;

use RRZE\PWA\Options;

/**
 * [Common description]
 */
class Common
{
    /**
     * [protected description]
     * @var object
     */
    protected $options;

    public function __construct()
    {
        $this->options = Options::getOptions();
    }

    /**
     * [getStartUrl description]
     * @param  boolean $rel [description]
     * @return string       [description]
     */
    public function getStartUrl($rel = false)
    {
        $startUrl = get_permalink($this->options->start_url) ? get_permalink($this->options->start_url) : get_bloginfo('url');

        $startUrl = $this->sanitizeHttp($startUrl);

        if ($rel === true) {
            $startUrl = (parse_url($startUrl, PHP_URL_PATH) == '') ? '.' : parse_url($startUrl, PHP_URL_PATH);
            return apply_filters('rrze_pwa_manifest_start_url', $startUrl);
        }

        return $startUrl;
    }

    /**
     * [getOfflinePage description]
     * @return string [description]
     */
    public function getOfflinePage()
    {
        return get_permalink($this->options->offline_page) ? $this->sanitizeHttp(get_permalink($this->options->offline_page)) : $this->sanitizeHttp(get_bloginfo('url'));
    }

    /**
     * [isResponseSuccessful description]
     * @param  string $url [description]
     * @return boolean       [description]
     */
    public function isResponseSuccessful($url)
    {
        $response = wp_remote_head($url, ['sslverify' => false]);
        if (wp_remote_retrieve_response_code($response) !== 200) {
            return false;
        }
        return true;
    }

    /**
     * [sanitizeHttp description]
     * @param string $url
     * @return string
     */
    public function sanitizeHttp($url)
    {
        return str_replace('http://', 'https://', $url);
    }
}
