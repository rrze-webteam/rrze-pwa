<?php

namespace RRZE\PWA;

defined('ABSPATH') || exit;

/**
 * [File description]
 */
class File
{
    /**
     * [wpFilesystem description]
     */
    public static function wpFilesystem()
    {
        global $wp_filesystem;

        if (empty($wp_filesystem)) {
            require_once(trailingslashit(ABSPATH) . 'wp-admin/includes/file.php');
            WP_Filesystem();
        }
    }

    /**
     * [putContents description]
     * @param  string $file    [description]
     * @param  string $content [description]
     * @return boolean          [description]
     */
    public static function putContents($file, $content = null)
    {
        if (empty($file)) {
            return false;
        }

        self::wpFilesystem();
        global $wp_filesystem;

        if (!$wp_filesystem->put_contents($file, $content, 0644)) {
            return false;
        }

        return true;
    }

    /**
     * [delete description]
     * @param  string $file [description]
     * @return boolean       [description]
     */
    public static function delete($file)
    {
        if (empty($file)) {
            return false;
        }

        self::wpFilesystem();
        global $wp_filesystem;

        return $wp_filesystem->delete($file);
    }
}
