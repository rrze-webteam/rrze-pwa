<?php

namespace RRZE\PWA;

defined('ABSPATH') || exit;

use RRZE\PWA\Options;
use RRZE\PWA\Common;

/**
 * [Manifest description]
 */
class Manifest
{
    /**
     * [protected description]
     * @var string
     */
    protected $optionName;

    /**
     * [protected description]
     * @var object
     */
    protected $options;

    /**
     * [protected description]
     * @var object
     */
    protected $common;

    /**
     * [protected description]
     * @var string
     */
    protected $filename = 'manifest.json';

    public function __construct()
    {
        $this->optionName = Options::getOptionName();
        $this->options = Options::getOptions();

        $this->common = new Common();
    }

    public function onLoaded()
    {
        add_action('wp_head', [$this, 'wpHead'], 0);
    }

    /**
     * [getFilename description]
     * @return string [description]
     */
    public function getFilename()
    {
        return apply_filters('rrze_pwa_manifest_filename', $this->filename);
    }

    /**
     * [getLocation description]
     * @return string      [description]
     */
    public function getLocation()
    {
        $filename = $this->getFilename();
        return home_url('/') . $filename;
    }

    /**
     * [build description]
     * @return boolean
     */
    public function build()
    {
        if ($this->common->isResponseSuccessful(home_url('/') . $this->getFilename())) {
            return true;
        }

        return false;
    }

    /**
     * [wpHead description]
     */
    public function wpHead()
    {
        $headTag = '<link rel="manifest" href="' . parse_url($this->getLocation(), PHP_URL_PATH) . '">' . PHP_EOL;

        if (apply_filters('rrze_pwa_add_theme_color', true)) {
            $headTag .= '<meta name="theme-color" content="' . $this->options->theme_color . '">' . PHP_EOL;
        }

        $headTag = apply_filters('rrze_pwa_head_tag', $headTag);

        echo $headTag;
    }

    /**
     * [getIcons description]
     * @return array [description]
     */
    protected function getIcons()
    {
        $iconsAry[] = [
            'src' => $this->options->icon,
            'sizes' => '192x192',
            'type' => 'image/png'
        ];

        if ($this->options->splash_icon != '') {
            $iconsAry[] = [
                'src' => $this->options->splash_icon,
                'sizes' => '512x512',
                'type' => 'image/png'
            ];
        }

        return $iconsAry;
    }

    /**
     * [getScope description]
     * @return string [description]
     */
    protected function getScope()
    {
        return parse_url(trailingslashit(get_bloginfo('url')), PHP_URL_PATH);
    }

    /**
     * [getOrientation description]
     * @return string [description]
     */
    protected function getOrientation()
    {
        $orientation = absint($this->options->orientation);

        switch ($orientation) {

            case 0:
                return 'any';
                break;

            case 1:
                return 'portrait';
                break;

            case 2:
                return 'landscape';
                break;

            default:
                return 'any';
        }
    }

    /**
     * [getDisplay description]
     * @return string [description]
     */
    protected function getDisplay()
    {
        $display = absint($this->options->display);

        switch ($display) {

            case 0:
                return 'fullscreen';
                break;

            case 1:
                return 'standalone';
                break;

            case 2:
                return 'minimal-ui';
                break;

            case 3:
                return 'browser';
                break;

            default:
                return 'standalone';
        }
    }

    /**
     * [template description]
     * @return array [description]
     */
    public function template()
    {
        $manifest = [];
        $manifest['name'] = $this->options->app_name;
        $manifest['short_name'] = $this->options->app_short_name;

        if (isset($this->options->description) && !empty($this->options->description)) {
            $manifest['description'] = $this->options->description;
        }

        $manifest['icons'] = $this->getIcons();
        $manifest['background_color'] = $this->options->background_color;
        $manifest['theme_color'] = $this->options->theme_color;
        $manifest['display'] = $this->getDisplay();
        $manifest['orientation'] = $this->getOrientation();
        $manifest['start_url'] = $this->common->getStartUrl(true);
        $manifest['scope'] = $this->getScope();

        return apply_filters('rrze_pwa_manifest', $manifest);
    }
}
