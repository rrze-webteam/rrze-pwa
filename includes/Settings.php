<?php

namespace RRZE\PWA;

defined('ABSPATH') || exit;

use RRZE\PWA\Options;
use RRZE\PWA\Common;
use RRZE\PWA\Manifest;
use RRZE\PWA\ServiceWorker;

/**
 * [Settings description]
 */
class Settings
{
    /**
     * [protected description]
     * @var string
     */
    protected $optionName;

    /**
     * [protected description]
     * @var object
     */
    protected $options;

    /**
     * [protected description]
     * @var object
     */
    protected $defaultOptions;

    /**
     * [protected description]
     * @var string
     */
    protected $settingsPageId;

    /**
     * [protected description]
     * @var object
     */
    protected $common;

    /**
     * [protected description]
     * @var object
     */
    protected $manifest;

    /**
     * [protected description]
     * @var object
     */
    protected $serviceWorker;

    /**
     * [__construct description]
     */
    public function __construct()
    {
        $this->optionName = Options::getOptionName();
        $this->defaultOptions = Options::getDefaultOptions();
        $this->options = Options::getOptions();

        $this->common = new Common();
        $this->manifest = new Manifest();
        $this->serviceWorker = new ServiceWorker();
    }

    /**
     * [onLoaded description]
     */
    public function onLoaded()
    {
        add_action('admin_enqueue_scripts', [$this, 'enqueueAdminScripts']);

        add_filter('plugin_action_links_' . plugin()->getBaseName(), [$this, 'pluginActionLink']);

        add_action('admin_menu', [$this, 'settingsMenu']);
        add_action('admin_init', [$this, 'settingsInit']);

        add_action('add_option_' . $this->optionName, [$this, 'rebuildFiles']);
        add_action('update_option_' . $this->optionName, [$this, 'rebuildFiles']);
    }

    /**
     * [enqueueAdminScripts description]
     */
    public function enqueueAdminScripts($hook)
    {
        if (strpos($hook, 'rrze-pwa') === false) {
            return;
        }

        wp_enqueue_style('wp-color-picker');
        wp_enqueue_media();

        wp_register_script(
            'rrze-pwa-settings',
            plugins_url('assets/js/settings.min.js', plugin()->getBasename()),
            ['wp-color-picker'],
            filemtime(plugin()->getPath('assets/js') . 'settings.min.js'),
            true
        );
        wp_localize_script(
            'rrze-pwa-settings',
            'RRZE_PWA',
            [
                'mediaTitle' => __('Splash Screen Icon', 'rrze-pwa'),
                'mediaBtnText' => __('Select Icon', 'rrze-pwa')
            ]
        );
        wp_enqueue_script('rrze-pwa-settings');
    }

    /**
     * [pluginActionLink description]
     * @param  array $links [description]
     * @return array        [description]
     */
    public function pluginActionLink($links)
    {
        if (!current_user_can('manage_options')) {
            return $links;
        }
        return array_merge($links, array(sprintf('<a href="%s">%s</a>', add_query_arg(array('page' => 'rrze-pwa'), admin_url('options-general.php')), __('Settings', 'rrze-pwa'))));
    }

    /**
     * [settingsMenu description]
     */
    public function settingsMenu()
    {
        $this->settingsPageId = add_options_page(__('PWA', 'rrze-pwa'), __('PWA', 'rrze-pwa'), 'manage_options', 'rrze-pwa', [$this, 'settingsPage']);
        //add_action('load-' . $this->settingsPageId, [$this, 'adminHelpMenu']);
    }

    /**
     * [settingsPage description]
     */
    public function settingsPage()
    {
?>
        <div class="wrap">
            <h2><?php echo __('PWA Settings', 'rrze-pwa'); ?></h2>
            <form method="post" action="options.php">
                <?php settings_fields('rrze_pwa_options'); ?>
                <?php do_settings_sections('rrze_pwa_options'); ?>
                <?php submit_button(); ?>
            </form>
        </div>
    <?php
    }

    /**
     * [settingsInit description]
     */
    public function settingsInit()
    {
        register_setting('rrze_pwa_options', $this->optionName, [$this, 'optionsValidate']);
        add_settings_section('rrze_pwa_section_1', false, '__return_false', 'rrze_pwa_options');
        add_settings_field('enabled', __('Enabled', 'rrze-pwa'), [$this, 'enabledField'], 'rrze_pwa_options', 'rrze_pwa_section_1');
        add_settings_field('status', __('Status', 'rrze-pwa'), [$this, 'statusField'], 'rrze_pwa_options', 'rrze_pwa_section_1');
        add_settings_field('app_name', __('Application Name', 'rrze-pwa'), [$this, 'appNameField'], 'rrze_pwa_options', 'rrze_pwa_section_1');
        add_settings_field('app_short_name', __('Application Short Name', 'rrze-pwa'), [$this, 'appShortNameField'], 'rrze_pwa_options', 'rrze_pwa_section_1');
        add_settings_field('description', __('Description', 'rrze-pwa'), [$this, 'descriptionField'], 'rrze_pwa_options', 'rrze_pwa_section_1');
        add_settings_field('icon', __('Application Icon', 'rrze-pwa'), [$this, 'iconField'], 'rrze_pwa_options', 'rrze_pwa_section_1');
        add_settings_field('splash_icon', __('Splash Screen Icon', 'rrze-pwa'), [$this, 'splashIconField'], 'rrze_pwa_options', 'rrze_pwa_section_1');
        //add_settings_field('background_color', __('Background Color', 'rrze-pwa'), [$this, 'backgroundColorField'], 'rrze_pwa_options', 'rrze_pwa_section_1');
        //add_settings_field('theme_color', __('Theme Color', 'rrze-pwa'), [$this, 'themeColorField'], 'rrze_pwa_options', 'rrze_pwa_section_1');
        add_settings_field('start_url', __('Start Page', 'rrze-pwa'), [$this, 'startUrlField'], 'rrze_pwa_options', 'rrze_pwa_section_1');
        add_settings_field('offline_page', __('Offline Page', 'rrze-pwa'), [$this, 'offlinePageField'], 'rrze_pwa_options', 'rrze_pwa_section_1');
        //add_settings_field('orientation', __('Orientation', 'rrze-pwa'), [$this, 'orientationField'], 'rrze_pwa_options', 'rrze_pwa_section_1');
        //add_settings_field('display', __('Display', 'rrze-pwa'), [$this, 'displayField'], 'rrze_pwa_options', 'rrze_pwa_section_1');       
    }

    /**
     * [optionsValidate description]
     * @param  mixed $input [description]
     * @return array        [description]
     */
    public function optionsValidate($input)
    {
        $input = (array) $input;

        $appName = isset($input['app_name']) ? sanitize_text_field($input['app_name']) : '';
        if (!empty($appName)) {
            $input['description'] = $appName;
        } else {
            add_settings_error(
                'rrze_pwa_options',
                'app_name',
                __('The Application Name must not be empty.', 'rrze-pwa'),
                'error'
            );
        }

        $appShortName = isset($input['app_short_name']) ? substr(sanitize_text_field($input['app_short_name']), 0, 12) : '';
        if (!empty($appShortName)) {
            $input['app_short_name'] = $appShortName;
        } else {
            add_settings_error(
                'rrze_pwa_options',
                'app_short_name',
                __('The Application Short Name must not be empty.', 'rrze-pwa'),
                'error'
            );
        }

        $description = isset($input['description']) ? sanitize_text_field($input['description']) : '';
        if (!empty($description)) {
            $input['description'] = $description;
        } else {
            add_settings_error(
                'rrze_pwa_options',
                'description',
                __('The description must not be empty.', 'rrze-pwa'),
                'error'
            );
        }

        $icon = isset($input['icon']) ? sanitize_text_field($input['icon']) : '';
        if ($this->validateIcon($icon, 192)) {
            $input['icon'] = $icon;
        } else {
            add_settings_error(
                'rrze_pwa_options',
                'icon',
                __('The Application Icon is not valid.', 'rrze-pwa'),
                'error'
            );
        }

        $splashIcon = isset($input['splash_icon']) ? sanitize_text_field($input['splash_icon']) : '';
        if ($this->validateIcon($splashIcon, 512)) {
            $input['splash_icon'] = $splashIcon;
        } else {
            add_settings_error(
                'rrze_pwa_options',
                'splashIcon',
                __('The Splash Screen Icon is not valid.', 'rrze-pwa'),
                'error'
            );
        }

        $backgroundColor = isset($input['background_color']) ? sanitize_text_field($input['background_color']) : '';
        $input['background_color'] = $this->validateColor($backgroundColor) ? $backgroundColor : $this->defaultOptions->background_color;

        $themeColor = isset($input['theme_color']) ? sanitize_text_field($input['theme_color']) : '';
        $input['theme_color'] = $this->validateColor($themeColor) ? $themeColor : $this->defaultOptions->theme_color;

        $input['start_url'] = isset($input['start_url']) ? absint($input['start_url']) : $this->defaultOptions->start_url;
        $input['offline_page'] = isset($input['offline_page']) ? absint($input['offline_page']) : $this->defaultOptions->offline_page;

        $input['orientation'] = isset($input['orientation']) ? absint($input['orientation']) : $this->defaultOptions->orientation;
        $input['display'] = isset($input['display']) ? absint($input['display']) : $this->defaultOptions->display;

        $input['enabled'] = !empty($input['enabled']) && !get_settings_errors('rrze_pwa_options') ? 1 : 0;

        add_action('admin_notices', function () {
            settings_errors('rrze_pwa_options', false, true);
        });

        return $input;
    }

    /**
     * [validateIcon description]
     * @param string $url [description]
     * @param integer $size [description]
     * @return boolean [description]
     */
    protected function validateIcon(string $url = '', int $size = 0): bool
    {
        if (strpos($url, home_url('/', 'https')) === false) {
            return false;
        }
        $response = wp_remote_get($url, ['sslverify' => false]);
        if (is_wp_error($response)) {
            return false;
        }
        if (wp_remote_retrieve_response_code($response) !== 200) {
            return false;
        }
        if (wp_remote_retrieve_header($response, 'content-type') != image_type_to_mime_type(IMAGETYPE_PNG)) {
            return false;
        }
        if (($body = wp_remote_retrieve_body($response)) == '') {
            return false;
        }
        if (($image = @imagecreatefromstring($body)) === false) {
            return false;
        }
        $dims = [imagesx($image), imagesy($image)];
        imagedestroy($image);
        if ($dims != [$size, $size]) {
            return false;
        }
        return true;
    }

    /**
     * [validateIcon description]
     * @param string $color [description]
     * @return boolean [description]
     */
    protected function validateColor(string $color = ''): bool
    {
        if (empty($color)) {
            return false;
        }
        if (!preg_match('/#([a-f0-9]{3}){1,2}\b/i', $color)) {
            return false;
        }
        return true;
    }

    /**
     * [enabledField description]
     */
    public function enabledField()
    {
    ?>
        <input type='checkbox' name="<?php printf('%s[enabled]', $this->optionName); ?>" value="1" <?php echo checked($this->options->enabled, 1); ?>>
        <span><?php _e('Enable Progressive Web App (PWA)', 'rrze-pwa'); ?></span>
    <?php
    }

    /**
     * [statusField description]
     */
    public function statusField()
    {
        if ($this->options->enabled && ($this->common->isResponseSuccessful($this->manifest->getLocation()) || $this->manifest->build())) {
            echo '<p><span class="dashicons dashicons-yes-alt" style="color: #007CBF"></span> ' . __('Manifest', 'rrze-pwa') . '</p>';
        } else {
            echo '<p><span class="dashicons dashicons-warning" style="color: #444"></span> ' . __('Manifest', 'rrze-pwa') . '</p>';
        }

        if ($this->options->enabled && ($this->common->isResponseSuccessful($this->serviceWorker->getLocation()) || $this->serviceWorker->build())) {
            echo '<p><span class="dashicons dashicons-yes-alt" style="color: #007CBF"></span> ' . __('Service Worker', 'rrze-pwa') . '</p>';
        } else {
            echo '<p><span class="dashicons dashicons-warning" style="color: #444"></span> ' . __('Service Worker', 'rrze-pwa') . '</p>';
        }
    }

    /**
     * [appNameField description]
     */
    public function appNameField()
    {
    ?>
        <input type='text' name="<?php printf('%s[app_name]', $this->optionName); ?>" class="regular-text" value="<?php echo esc_attr($this->options->app_name); ?>">
        <p class="description">
            <?php _e("The Application Name is used in the app install prompt.", 'rrze-pwa'); ?>
        </p>
    <?php
    }

    /**
     * [appShortNameField description]
     */
    public function appShortNameField()
    {
    ?>
        <input type='text' name="<?php printf('%s[app_short_name]', $this->optionName); ?>" class="regular-text" value="<?php echo esc_attr($this->options->app_short_name); ?>">
        <p class="description">
            <?php _e("The Application Short Name is used on the user's home screen, launcher, or other places where space may be limited. Up to 12 characters.", 'rrze-pwa'); ?>
        </p>
    <?php
    }

    /**
     * [descriptionField description]
     */
    public function descriptionField()
    {
    ?>
        <input type='text' name="<?php printf('%s[description]', $this->optionName); ?>" class="regular-text" value="<?php echo esc_attr($this->options->description); ?>">
        <p class="description">
            <?php _e('A brief description of what the applicaton is about.', 'rrze-pwa'); ?>
        </p>
    <?php
    }

    /**
     * [iconField description]
     */
    public function iconField()
    {
    ?>
        <input type='text' name="<?php printf('%s[icon]', $this->optionName); ?>" class="rrze-pwa-icon regular-text" value="<?php echo esc_attr($this->options->icon); ?>">
        <button type="button" class="button rrze-pwa-icon-upload" data-editor="content">
            <span class="dashicons dashicons-format-image" style="margin-top: 4px;"></span> <?php _e('Choose Icon', 'rrze-pwa'); ?>
        </button>

        <p class="description">
            <?php _e('This will be the app icon when installed on the device. It is a PNG image with an exact size of 192x192 pixels saved in the media library.', 'rrze-pwa'); ?>
        </p>
    <?php
    }

    /**
     * [splashIconField description]
     */
    public function splashIconField()
    {
    ?>
        <input type='text' name="<?php printf('%s[splash_icon]', $this->optionName); ?>" class="rrze-pwa-splash-icon regular-text" value="<?php echo esc_attr($this->options->splash_icon); ?>">
        <button type="button" class="button rrze-pwa-splash-icon-upload" data-editor="content">
            <span class="dashicons dashicons-format-image" style="margin-top: 4px;"></span> <?php _e('Choose Icon', 'rrze-pwa'); ?>
        </button>

        <p class="description">
            <?php _e('This icon will be displayed on the splash screen of the application on supported devices. It is a PNG image with an exact size of 512x512 pixels saved in the media library.', 'rrze-pwa'); ?>
        </p>
    <?php
    }

    /**
     * [backgroundColorField description]
     */
    public function backgroundColorField()
    {
    ?>
        <input type='text' name="<?php printf('%s[background_color]', $this->optionName); ?>" class="rrze-pwa-colorpicker" value="<?php echo esc_attr($this->options->background_color); ?>" data-default-color="#D5E0EB">

        <p class="description">
            <?php _e('The Background Color property is used on the splash screen when the application is first launched.', 'rrze-pwa'); ?>
        </p>
    <?php
    }

    /**
     * [themeColorField description]
     */
    public function themeColorField()
    {
    ?>
        <input type='text' name="<?php printf('%s[theme_color]', $this->optionName); ?>" class="rrze-pwa-colorpicker" value="<?php echo esc_attr($this->options->theme_color); ?>" data-default-color="#D5E0EB">

        <p class="description">
            <?php _e("The Theme Color sets the color of the tool bar, and may be reflected in the app's preview in task switchers.", 'rrze-pwa'); ?>
        </p>
    <?php
    }

    /**
     * [startUrlField description]
     */
    public function startUrlField()
    {
    ?>
        <fieldset>
            <label for="start-url">
                <?php echo wp_dropdown_pages([
                    'name' => sprintf('%s[start_url]', $this->optionName),
                    'echo' => 0,
                    'show_option_none' => __('&mdash; Homepage &mdash;', 'rrze-pwa'),
                    'option_none_value' => '0',
                    'selected' => $this->options->start_url
                ]); ?>
            </label>

            <p class="description">
                <?php printf(
                    __(
                        /* translators: %s: Current Start Page Url. */
                        'The Start Page tells the browser where the application should start when it is launched. Current Start Page Url is %s',
                        'rrze-pwa'
                    ),
                    $this->common->getStartUrl()
                ); ?>
            </p>
        </fieldset>
    <?php
    }

    /**
     * [offlinePageField description]
     */
    public function offlinePageField()
    {
    ?>
        <label for="offline-page">
            <?php echo wp_dropdown_pages([
                'name' => sprintf('%s[offline_page]', $this->optionName),
                'echo' => 0,
                'show_option_none' => __('&mdash; Default &mdash;', 'rrze-pwa'),
                'option_none_value' => '0',
                'selected' => $this->options->offline_page
            ]); ?>
        </label>

        <p class="description">
            <?php printf(__(
                /* translators: %s: Current Offline Page Url. */
                'The Offline Page is displayed when offline and the requested page is not already cached. Current Offline Page Url is %s',
                'rrze-pwa'
            ), $this->common->getOfflinePage()); ?>
        </p>
    <?php
    }

    /**
     * [orientationField description]
     */
    public function orientationField()
    {
    ?>
        <label for="orientation">
            <select name="<?php printf('%s[orientation]', $this->optionName); ?>" id="rrze-pwa-orientation">
                <option value="0" <?php selected($this->options->orientation, 0); ?>>
                    <?php _e('Follow Device Orientation', 'rrze-pwa'); ?>
                </option>
                <option value="1" <?php selected($this->options->orientation, 1); ?>>
                    <?php _e('Portrait', 'rrze-pwa'); ?>
                </option>
                <option value="2" <?php selected($this->options->orientation, 2); ?>>
                    <?php _e('Landscape', 'rrze-pwa'); ?>
                </option>
            </select>
        </label>

        <p class="description">
            <?php _e('A specific orientation can be applied, which is advantageous for applications that work in a single orientation.', 'rrze-pwa'); ?>
        </p>
    <?php
    }

    /**
     * [displayField description]
     */
    public function displayField()
    {
    ?>
        <label for="display">
            <select name="<?php printf('%s[display]', $this->optionName); ?>" id="rrzw_pwa_display">
                <option value="0" <?php selected($this->options->display, 0); ?>>
                    <?php _e('Full Screen', 'rrze-pwa'); ?>
                </option>
                <option value="1" <?php selected($this->options->display, 1); ?>>
                    <?php _e('Standalone', 'rrze-pwa'); ?>
                </option>
                <option value="2" <?php selected($this->options->display, 2); ?>>
                    <?php _e('Minimal UI', 'rrze-pwa'); ?>
                </option>
                <option value="3" <?php selected($this->options->display, 3); ?>>
                    <?php _e('Browser', 'rrze-pwa'); ?>
                </option>
            </select>
        </label>

        <p class="description">
            <?php _e('Display mode decides what browser UI is shown when the application is launched. Standalone is default.', 'rrze-pwa'); ?>
        </p>
<?php
    }

    /**
     * [adminHelpMenu description]
     */
    public function adminHelpMenu()
    {
        $content = [
            '<p>' . __('Here comes the Context Help content.', 'rrze-pwa') . '</p>',
        ];

        $helpTab = [
            'id' => $this->settingsPageId,
            'title' => __('Overview', 'rrze-pwa'),
            'content' => implode(PHP_EOL, $content),
        ];

        $helpSidebar = sprintf(
            '<p><strong>%1$s:</strong></p><p><a href="http://blogs.fau.de/webworking">RRZE Webworking</a></p><p><a href="https://github.com/RRZE Webteam">%2$s</a></p>',
            __('For more information', 'rrze-pwa'),
            __('RRZE Webteam on Github', 'rrze-pwa')
        );

        $screen = get_current_screen();

        if ($screen->id != $this->settingsPageId) {
            return;
        }

        $screen->add_help_tab($helpTab);

        $screen->set_help_sidebar($helpSidebar);
    }

    /**
     * [rebuildFiles description]
     */
    public function rebuildFiles()
    {
        if ($this->options->enabled) {
            $this->manifest->build();
            $this->serviceWorker->build();
        }
    }
}
