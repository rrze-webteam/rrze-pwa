<?php

namespace RRZE\PWA;

defined('ABSPATH') || exit;

/**
 * [Options description]
 */
class Options
{
    /**
     * [protected description]
     * @var string
     */
    protected static $optionName = 'rrze_pwa';

    /**
     * [defaultOptions description]
     * @return array [description]
     */
    protected static function defaultOptions()
    {
        $blogName = get_bloginfo('name');

        $options = [
            'enabled' => 0,
            'app_name' => $blogName ? $blogName : '',
            'app_short_name' => $blogName ? substr($blogName, 0, 12) : '',
            'description' => get_bloginfo('description'),
            'icon' => '',
            'splash_icon' => '',
            'background_color' => '#FFFFFF',
            'theme_color' => '#FFFFFF',
            'start_url' => 0,
            'offline_page' => 0,
            'orientation' => 1,
            'display' => 1
        ];

        return $options;
    }

    /**
     * [getDefaultOptions description]
     * @return object [description]
     */
    public static function getDefaultOptions()
    {
        return (object) self::defaultOptions();
    }

    /**
     * [getOptions description]
     * @return object [description]
     */
    public static function getOptions()
    {
        $defaults = self::defaultOptions();

        $options = (array) get_option(self::$optionName);
        $options = wp_parse_args($options, $defaults);
        $options = array_intersect_key($options, $defaults);

        return (object) $options;
    }

    /**
     * [getOptionName description]
     * @return string [description]
     */
    public static function getOptionName()
    {
        return self::$optionName;
    }
}
