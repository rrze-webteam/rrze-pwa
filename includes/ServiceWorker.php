<?php

namespace RRZE\PWA;

defined('ABSPATH') || exit;

use RRZE\PWA\Options;
use RRZE\PWA\Common;
use RRZE\PWA\Minifier;

/**
 * [ServiceWorker description]
 */
class ServiceWorker
{
    /**
     * Optionsname
     * @var string
     */
    protected $optionName;

    /**
     * [protected description]
     * @var object
     */
    protected $options;

    /**
     * Optionsname
     * @var object
     */
    protected $common;

    /**
     * [protected description]
     * @var string
     */
    protected $filename = 'sw.js';

    public function __construct()
    {
        $this->optionName = Options::getOptionName();
        $this->options = Options::getOptions();
        $this->common = new Common();
    }

    public function onLoaded()
    {
        add_action('wp_enqueue_scripts', [$this, 'enqueueScripts']);
        add_filter('rrze_pwa_files_to_cache', [$this, 'offlinePageImages']);
    }

    /**
     * [enqueueScripts description]
     */
    public function enqueueScripts()
    {
        wp_enqueue_script(
            'rrze-pwa',
            plugins_url('assets/js/plugin.min.js', plugin()->getBasename()),
            [],
            filemtime(plugin()->getPath('assets/js') . 'plugin.min.js'),
            true
        );

        wp_localize_script(
            'rrze-pwa',
            'RRZE_PWA',
            [
                'url' => parse_url($this->getLocation(), PHP_URL_PATH),
            ]
        );
    }

    /**
     * [getFilename description]
     * @return string [description]
     */
    public function getFilename()
    {
        return apply_filters('rrze_pwa_sw_filename', $this->filename);
    }

    /**
     * [getLocation description]
     * @return string      [description]
     */
    public function getLocation()
    {
        $filename = $this->getFilename();
        return home_url('/') . $filename;
    }

    /**
     * [build description]
     * @return boolean [description]
     */
    public function build()
    {
        if ($this->common->isResponseSuccessful(home_url('/') . $this->getFilename())) {
            return true;
        }

        return false;
    }

    /**
     * [offlinePageImages description]
     * @param  string $filesToCache [description]
     * @return string                 [description]
     */
    public function offlinePageImages($filesToCache)
    {
        $post = get_post($this->options->offline_page);

        if ($post === null) {
            return $filesToCache;
        }

        preg_match_all('/<img[^>]+src="([^">]+)"/', $post->post_content, $matches);

        if (!empty($matches[1])) {
            return $this->common->sanitizeHttp($filesToCache . ', \'' . implode('\', \'', $matches[1]) . '\'');
        }

        return $filesToCache;
    }

    /**
     * [template description]
     * @return string [description]
     */
    public function template()
    {
        ob_start(); ?>
        'use strict';

        const cacheName = '<?php echo parse_url(get_bloginfo('url'), PHP_URL_HOST) . ' - rrze - pwa - ' . plugin()->getVersion(); ?>';
        const startPage = '<?php echo $this->common->getStartUrl(); ?>';
        const offlinePage = '<?php echo $this->common->getOfflinePage(); ?>';
        const filesToCache = [<?php echo apply_filters('rrze_pwa_files_to_cache', 'startPage, offlinePage'); ?>];
        const skipCacheUrls = [<?php echo apply_filters('rrze_pwa_skip_cache_urls', '/\/wp-admin/,/\/wp-login/,/\/wp-json/,/preview=true/'); ?>];

        // Install
        self.addEventListener('install', function (e) {
            console.log('Service Worker installation.');
            e.waitUntil(
                caches.open(cacheName).then(function (cache) {
                    console.log('Service Worker caching dependencies.');
                    filesToCache.map(function (url) {
                        return cache.add(url).catch(function (reason) {
                            return console.log('RRZE-PWA: ' + String(reason) + ' ' + url);
                        });
                    });
                })
            );
        });

        // Activate
        self.addEventListener('activate', function (e) {
            console.log('Service Worker activation.');
            e.waitUntil(
                caches.keys().then(function (keyList) {
                    return Promise.all(keyList.map(function (key) {
                        if (key !== cacheName) {
                            console.log('RRZE-PWA old cache removed', key);
                            return caches.delete(key);
                        }
                    }));
                })
            );
            return self.clients.claim();
        });

        // Fetch
        self.addEventListener('fetch', function (e) {

            // Return if the current request url is in the never cache list
            if (!skipCacheUrls.every(checkSkipCacheList, e.request.url)) {
                console.log('Service Worker: Current request is excluded from cache.');
                return;
            }

            // Return if request url protocal is not http or https
            if (!e.request.url.match(/^(http|https):\/\//i))
                return;

            // Return if request url is from an external domain.
            if (new URL(e.request.url).origin !== location.origin)
                return;

            // For POST requests, do not use the cache. Serve offline page if offline.
            if (e.request.method !== 'GET') {
                e.respondWith(
                    fetch(e.request).catch(function () {
                        return caches.match(offlinePage);
                    })
                );
                return;
            }

            // Refresh cache
            if (e.request.mode === 'navigate' && navigator.onLine) {
                e.respondWith(
                    fetch(e.request).then(function (response) {
                        return caches.open(cacheName).then(function (cache) {
                            cache.put(e.request, response.clone());
                            return response;
                        });
                    })
                );
                return;
            }

            e.respondWith(
                caches.match(e.request).then(function (response) {
                    return response || fetch(e.request).then(function (response) {
                        return caches.open(cacheName).then(function (cache) {
                            cache.put(e.request, response.clone());
                            return response;
                        });
                    });
                }).catch(function () {
                    return caches.match(offlinePage);
                })
            );
        });

        // Check if current url is in the skipCacheUrls list
        function checkSkipCacheList(url) {
            if (this.match(url)) {
                return false;
            }
            return true;
        }
        <?php
        return apply_filters('rrze_pwa_sw_template', Minifier::minify(ob_get_clean()));
    }    
}
