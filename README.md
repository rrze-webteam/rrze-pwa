# Progressive Web-App (PWA) Plugin für WordPress
Dieses Plugin erweitert die aktuelle Website zu einer progressiven Web-App.

## Browser-Kompatibilität
Die Browser, die derseit Web-Apps unterstützen, sind:
* Chrome Version 81 und höher (Android und Desktop)
* Firefox Version 68 und höher (Android)
* Edge Version 81 und höher (Desktop)

## Einstellungen
Sobald das Plugin aktiviert ist, muss es im Menü Einstellungen / PWA konfiguriert werden.

Die Einstellungen generiert die erforderliche Manifest-Datei (/manifest.json) und den Service-Worker (Javascript), damit der Browser die Web-App auf dem Startbildschirm installieren kann. Eine nachfolgende Änderung der Einstellungen wird nicht sofort im Browser angezeigt. In vielen Fällen muss zusätzlich zur Deinstallation der Web-App die Registrierung des Service-Workers aufgehoben und der Browser-Cache vollständig gelöscht werden. Derzeit hat nur der Chrome Browser für Android die Funktion, die Web-App basierend auf der Manifest-Datei (/manifest.json) zu aktualisieren.

Hinweis: Solange das Plugin nicht richtig konfiguriert ist, ist die PWA-Funktion für die Website nicht verfügbar.
